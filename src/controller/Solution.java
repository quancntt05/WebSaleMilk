package controller;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

import net.bytebuddy.utility.RandomString;

public class Solution {
	public static int findFirstSubstringOccurrence(String s, String x) {
		    int len=s.length()-x.length();
		    char[] arrS = s.toCharArray();
		    char[] arrX = x.toCharArray();
		    if(len<0)
		        return -1;
		    boolean ok=true;
		    for(int i=0; i<=len; i++){
		        ok = true;
		        for(int j=0; j< x.length(); j++){
		            if(arrS[i+j]!=arrX[j]){
		                ok=false;
		                break;
		            }
		        }
		        if(ok){
		            return i;
		        }
		    }
		    return -1;
		}


    public static void main(String[] args) {

    	long startTime = System.nanoTime();
    	RandomString randomString = new RandomString(1000000);
    	System.out.println(findFirstSubstringOccurrence(randomString.nextString(), "aaaa"));
//    	System.out.println(findFirstSubstringOccurrence("CodefightsIsAwesome", "IsA"));    	
    	long endTime = System.nanoTime();
    	System.out.println("Result: "+(endTime-startTime));
    }
}

