package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import JavaBeans.KhachHang;
import businesslogic.KhachHangBL;

@WebServlet("/ListCustomerController")
public class ListCustomerController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public ListCustomerController() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        try {
			List<KhachHang> lskh = KhachHangBL.docTatCa();
			request.setAttribute("lskh", lskh);
			request.getRequestDispatcher("jsp/listcustomer.jsp").forward(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
