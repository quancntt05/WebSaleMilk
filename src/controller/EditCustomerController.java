package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import JavaBeans.KhachHang;
import businesslogic.KhachHangBL;

@WebServlet("/EditCustomerController")
public class EditCustomerController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public EditCustomerController() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
		String id = request.getParameter("id");
		String action =request.getParameter("action");
		KhachHang khachHang;
		try {
			if(action!=null) {
				int resultsql = KhachHangBL.delCustomer(id);
				if (resultsql == 1) {
					request.setAttribute("result", "Xóa thành công");
				}else {
					request.setAttribute("result", "Xóa không thành công");
				}
				List<KhachHang> lskh = KhachHangBL.docTatCa();
				request.setAttribute("lskh", lskh);
				request.getRequestDispatcher("jsp/listcustomer.jsp").forward(request, response);
			}
			else {
				khachHang = KhachHangBL.timKHTheoId(id);
				request.setAttribute("txtID", khachHang.getMa_khach_hang());
				request.setAttribute("txtName", khachHang.getTen_khach_hang());
				request.setAttribute("txtGender", khachHang.getPhai());
				request.setAttribute("txtAddress", khachHang.getDia_chi());
				request.setAttribute("txtPhone", khachHang.getDien_thoai());
				request.setAttribute("txtEmail", khachHang.getEmail());
				if(request.getParameter("btnSummit")!=null){
					int resultsql = KhachHangBL.editKHTheoID(
							request.getParameter("txtID"),
							request.getParameter("txtName"),
							Integer.parseInt(request.getParameter("txtGender")),
							request.getParameter("txtAddress"),
							request.getParameter("txtPhone"),
							request.getParameter("txtEmail")
							);
					if (resultsql == 1) {
						request.setAttribute("result", "Cập nhật thành công");
						List<KhachHang> lskh = KhachHangBL.docTatCa();
						request.setAttribute("lskh", lskh);
						request.getRequestDispatcher("jsp/listcustomer.jsp").forward(request, response);
					}else {
						request.setAttribute("result", "Cập nhật không thành công");
						request.getRequestDispatcher("jsp/editcustomer.jsp").forward(request, response);
					}
				};
			request.getRequestDispatcher("jsp/editcustomer.jsp").forward(request, response);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
