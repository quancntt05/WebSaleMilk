package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import businesslogic.HangSuaBL;
import businesslogic.LoaiSuaBL;
import businesslogic.SuaBL;
import JavaBeans.HangSua;
import JavaBeans.LoaiSua;

@WebServlet({ "/AddProductController", "/them-sua.htm" })
public class AddProductController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public AddProductController() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        String maLoai, maHang, tenSua, maSua, tpDinhDuong, loiIch, hinhAnh;
        int trongLuong, donGia;
        

        
		try {
			List<LoaiSua> dsls = LoaiSuaBL.docTatCa();
			List<HangSua> dshs = HangSuaBL.docTatCa();
	        request.setAttribute("dsls", dsls);
	        request.setAttribute("dshs", dshs);
	        if(request.getParameter("btnSubmit")!=null) {
	            maLoai =request.getParameter("cboLoai");
	            maHang = request.getParameter("cboHang");
	            maSua = request.getParameter("txtMaSuaMoi");
	            tenSua = request.getParameter("txtTenSuaMoi");
	            trongLuong = Integer.parseInt(request.getParameter("txtTrongLuong"));
	            donGia = Integer.parseInt(request.getParameter("txtDonGia"));
	            tpDinhDuong = request.getParameter("txtTPDinhDuong");
	            loiIch = request.getParameter("txtLoiIch");
	            hinhAnh = request.getParameter("txtHinh");
	            
	        String result = SuaBL.themSuaMoi(maSua, tenSua, maHang, maLoai, donGia, trongLuong, tpDinhDuong, loiIch, hinhAnh);
	        request.setAttribute("result", result);
	        }
	        request.getRequestDispatcher("jsp/addproduct.jsp").forward(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}
        
	}

}
