package controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import services.ConnectDatabase;

@SuppressWarnings("serial")
public class FindProductController extends HttpServlet {
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String keySearch = req.getParameter("nameproduct");
		StringBuilder builder = new StringBuilder();
		ConnectDatabase database = new ConnectDatabase();
		Connection connection = database.connect();
		try {
			PreparedStatement smt = connection.prepareStatement("SELECT * FROM sua WHERE ten_sua like '%"+keySearch+"%'");
			ResultSet resultSet = smt.executeQuery();
			while (resultSet.next()) {
				builder.append(resultSet.getString("ten_sua")+"<br>"+"\n");
			}
			req.setAttribute("result", builder.toString());
			connection.close();
			RequestDispatcher dispatcher = req.getRequestDispatcher(req.getServletPath()+".jsp");
			dispatcher.forward(req, resp);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		RequestDispatcher dispatcher = req.getRequestDispatcher(req.getServletPath()+".jsp");
		dispatcher.forward(req, resp);
	}
}
