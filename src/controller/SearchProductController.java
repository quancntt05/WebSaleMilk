package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import businesslogic.HangSuaBL;
import businesslogic.LoaiSuaBL;
import businesslogic.SuaBL;
import JavaBeans.HangSua;
import JavaBeans.LoaiSua;
import JavaBeans.Sua;

@WebServlet({ "/SearchProductController", "/tim-sua.htm" })
public class SearchProductController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public SearchProductController() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        String maLoai, maHang, tenSua;
        
        maLoai=request.getParameter("cboLoai");
        maHang = request.getParameter("cboHang");
        tenSua = request.getParameter("txtTenSuaTim");
        
        
		try {
			List<LoaiSua> dsls = LoaiSuaBL.docTatCa();
			List<HangSua> dshs = HangSuaBL.docTatCa();
	        List<Sua> dsSua = SuaBL.timTheoLoaiHangTen(maLoai,maHang,tenSua);
	        HangSua hangSua = HangSuaBL.docTheoMaHang(maHang);
	        request.setAttribute("dsls", dsls);
	        request.setAttribute("dshs", dshs);
	        request.setAttribute("dsSua", dsSua);
	        request.setAttribute("soSP", dsSua.size());
	        request.setAttribute("hangSua", hangSua);
	        request.getRequestDispatcher("jsp/searchproduct.jsp").forward(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}
        
	}

}