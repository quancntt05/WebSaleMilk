package JavaBeans;

public class HangSua {
	private String maHangSua;
	private String tenHangSua;
	
	
	public String getMaHangSua() {
		return maHangSua;
	}
	public void setMaHangSua(String maHangSua) {
		this.maHangSua = maHangSua;
	}
	public String getTenHangSua() {
		return tenHangSua;
	}
	public void setTenHangSua(String tenHangSua) {
		this.tenHangSua = tenHangSua;
	}

	
}
