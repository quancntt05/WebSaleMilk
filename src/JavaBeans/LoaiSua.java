package JavaBeans;

public class LoaiSua {
	private String maLoaiSua;
	private String tenLoaiSua;
	
	public String getMaLoaiSua() {
		return maLoaiSua;
	}
	public void setMaLoaiSua(String maLoaiSua) {
		this.maLoaiSua = maLoaiSua;
	}
	public String getTenLoaiSua() {
		return tenLoaiSua;
	}
	public void setTenLoaiSua(String tenLoaiSua) {
		this.tenLoaiSua = tenLoaiSua;
	}

}
