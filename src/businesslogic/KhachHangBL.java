package businesslogic;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import JavaBeans.KhachHang;
import services.ConnectDatabase;

public class KhachHangBL {
	public static List<KhachHang> docTatCa() throws SQLException{
		List<KhachHang> lskh = new ArrayList<>();
		ConnectDatabase database = new ConnectDatabase();
		Connection connection = database.connect();
		String sql = "Select * from khach_hang";
		Statement statement = connection.createStatement();
		ResultSet resultSet = statement.executeQuery(sql);
		while (resultSet.next()) {
			KhachHang kh = new KhachHang();
			kh.setMa_khach_hang(resultSet.getString("ma_khach_hang"));
			kh.setTen_khach_hang(resultSet.getString("ten_khach_hang"));
			kh.setDia_chi(resultSet.getString("dia_chi"));
			kh.setDien_thoai(resultSet.getString("dien_thoai"));
			kh.setEmail(resultSet.getString("email"));
			kh.setPhai(resultSet.getInt("phai"));
			lskh.add(kh);
		}
		return lskh;
	}
	
	public static KhachHang timKHTheoId(String id) throws SQLException {
		KhachHang kh = new KhachHang();
		ConnectDatabase db = new ConnectDatabase();
		Connection connection = db.connect();
		String sql = "select * from khach_hang where ma_khach_hang = '"+id+"'";
		Statement stm = connection.createStatement();
		ResultSet resultSet = stm.executeQuery(sql);
		while(resultSet.next()) {
			kh.setMa_khach_hang(resultSet.getString("ma_khach_hang"));
			kh.setTen_khach_hang(resultSet.getString("ten_khach_hang"));
			kh.setDia_chi(resultSet.getString("dia_chi"));
			kh.setDien_thoai(resultSet.getString("dien_thoai"));
			kh.setEmail(resultSet.getString("email"));
			kh.setPhai(resultSet.getInt("phai"));
		}
		return kh;
	}
	
	public static int delCustomer(String id) throws SQLException {
		int result = 0;
		ConnectDatabase db = new ConnectDatabase();
		Connection connection = db.connect();
		String sql = "delete from khach_hang where ma_khach_hang = '"+id+"'";
		Statement stm = connection.createStatement();
		int query = stm.executeUpdate(sql);
		if (query>0) {
			return result = 1;
		}
		return result;
	}
	
	public static int editKHTheoID(String id,String name, int phai, String dia_chi,String sdt, String email) throws SQLException {
		int result = 0;
		ConnectDatabase db = new ConnectDatabase();
		Connection connection = db.connect();
		String sql = "update khach_hang set ten_khach_hang ='"+name+
				"', phai = '"+phai+"', dia_chi = '"+dia_chi+"',dien_thoai = '"+sdt+
				"', email = '"+email+"' where ma_khach_hang = '"+id+"'";
		Statement stm = connection.createStatement();
		int query = stm.executeUpdate(sql);
		if (query>0) {
			return result = 1;
		}
		return result;
	}
}
