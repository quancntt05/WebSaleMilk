package businesslogic;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import JavaBeans.HangSua;
import services.ConnectDatabase;

public class HangSuaBL {
	@SuppressWarnings("null")
	public static List<HangSua> docTatCa() throws SQLException{
		List<HangSua> lshs = new ArrayList<>();
		ConnectDatabase database = new ConnectDatabase();
		Connection connection = database.connect();
		String sql = "Select * from hang_sua";
		Statement statement = connection.createStatement();
		ResultSet resultSet = statement.executeQuery(sql);
		while (resultSet.next()) {
			HangSua hangSua = new HangSua();
			hangSua.setMaHangSua(resultSet.getString("ma_hang_sua"));
			hangSua.setTenHangSua(resultSet.getString("ten_hang_sua"));
			lshs.add(hangSua);
		}
		connection.close();
		return lshs;
	}

	public static HangSua docTheoMaHang(String maHang) throws SQLException {
		HangSua hangSua = null;
		ConnectDatabase connectDatabase = new ConnectDatabase();
		Connection connection = connectDatabase.connect();
		String sql = "Select * from hang_sua where ma_hang_sua = '"+maHang+"'";
		Statement statement = connection.createStatement();
		ResultSet resultSet = statement.executeQuery(sql);
		while (resultSet.next()) {
			hangSua = new HangSua();
			hangSua.setTenHangSua(resultSet.getString("ten_hang_sua"));
		}
		connection.close();
		return hangSua;
	}
}
