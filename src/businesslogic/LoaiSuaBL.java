package businesslogic;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import JavaBeans.LoaiSua;
import services.ConnectDatabase;

public class LoaiSuaBL {
	@SuppressWarnings("null")
	public static List<LoaiSua> docTatCa() throws SQLException{
		List<LoaiSua> lsls = new ArrayList<>();
		ConnectDatabase database = new ConnectDatabase();
		Connection connection = database.connect();
		String sql = "Select * from loai_sua";
		Statement statement = connection.createStatement();
		ResultSet resultSet = statement.executeQuery(sql);
		while (resultSet.next()) {
			LoaiSua loaiSua = new LoaiSua();
			loaiSua.setMaLoaiSua(resultSet.getString("ma_loai_sua"));
			loaiSua.setTenLoaiSua(resultSet.getString("ten_loai"));
			lsls.add(loaiSua);
		}
		connection.close();
		return lsls;
	}
}
