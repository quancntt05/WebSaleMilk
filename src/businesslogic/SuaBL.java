package businesslogic;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import JavaBeans.Sua;
import services.ConnectDatabase;

public class SuaBL {
	@SuppressWarnings("null")
	public static List<Sua> timTheoLoaiHangTen(String maLoai, String maHang, String tenSua) throws SQLException{
		List<Sua> listSua = new ArrayList<>();
		ConnectDatabase database = new ConnectDatabase();
		Connection connection = database.connect();
		String sql = "Select * from sua where ma_hang_sua = '"+maHang+"' and ma_loai_sua = '"+maLoai+"' and ten_sua like '%"+tenSua+"%'";
		Statement statement = connection.createStatement();
		ResultSet resultSet = statement.executeQuery(sql);
		while (resultSet.next()) {
			Sua sua = new Sua();
			sua.setTenSua(resultSet.getString("ten_sua"));
			sua.setHinh(resultSet.getString("hinh"));
			sua.setTrongLuong(resultSet.getInt("trong_luong"));
			sua.setDonGia(resultSet.getInt("don_gia"));
			sua.setTpDinhDuong(resultSet.getString("tp_dinh_duong"));
			sua.setLoiIch(resultSet.getString("loi_ich"));
			listSua.add(sua);
		}
		connection.close();
		return listSua;
		
	}
	public static String themSuaMoi(String id, String name, String maHangSua, String maLoai, int donGia, int trongLuong,
			String tpDinhDuong, String loiIch, String hinh) throws SQLException {
		String result = "Unsuccess add product";
		String sql = "INSERT INTO sua (ma_sua,ten_sua,ma_hang_sua,ma_loai_sua,trong_luong,don_gia,tp_dinh_duong,loi_ich,hinh) VALUES " + 
				"('"+id+"','"+name+"','"+maHangSua+"','"+maLoai+"','"+trongLuong+"','"+donGia+"','"+tpDinhDuong+"','"+loiIch+"','"+hinh+"')";
		ConnectDatabase database = new ConnectDatabase();
		Connection connection = database.connect();
		Statement statement = connection.createStatement();
		int resultSet = statement.executeUpdate(sql);
		if(resultSet > 0) {
			result = "Success add product";
		}
		return result;
	}
}
