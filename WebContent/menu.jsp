<%-- 
    Document   : danh-muc-san-pham
--%>

<%@page import="services.ConnectDatabase"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%!
		ConnectDatabase database = new ConnectDatabase();
		Connection con = database.connect();
    Statement stm = null;
    ResultSet rs = null;
%>
<%
    stm = con.createStatement();
    String maHang, maLoai;
    maHang = request.getParameter("maHang");
    maLoai = request.getParameter("maLoai");
    if(maHang!=null)
        rs = stm.executeQuery("select * from sua where ma_hang_sua='"+maHang+"'");
    else if(maLoai!=null)
        rs = stm.executeQuery("select * from sua where ma_loai_sua='"+maLoai+"'");
    else        
        rs = stm.executeQuery("select * from sua");
%>
<table border="1" width="100%">
    <thead>
        <tr>
            <th>Hình</th>
            <th>Tên sữa</th>
            <th>Trọng lượng (gr)</th>
            <th>Đơn giá (VNĐ)</th>
        </tr>
    </thead>
    <tbody>
        <%while(rs.next()){%>
        <tr>
            <td><img src="img/<%=rs.getString("hinh")%>"></td>
            <td><%= rs.getString("ten_sua")%></td>
            <td><%= rs.getInt("trong_luong")%></td>
            <td><%= rs.getInt("don_gia")%></td>
        </tr>
        <%}%>
    </tbody>
</table>
