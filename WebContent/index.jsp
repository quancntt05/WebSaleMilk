<%-- 
    Document   : trang-chu
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Trang chủ</title>
        <link rel="stylesheet" href="css/styles.css" type="text/css">
    </head>
    <body>
        <div id="container">
            <div id="header">
                <%@include  file="trang-dau.html"%>
            </div>
            <div id="content">
                <div id="col_1">
                    <p>Sản phẩm theo hãng sữa</p>
                    <jsp:include page="menu_hangsua.jsp"/>
                    <p>Sản phẩm theo loại sữa</p>
                    <jsp:include page="menu_loaisua.jsp"/>
                </div>
                <div id="col_2">
                    <jsp:include page="menu.jsp"/>
                </div>
                <div id="col_3">
                    <%@include file="trang-phai.html"%>
                </div>
            </div>
            <div id="footer">
                <%@include file="end_page.jsp"%>
            </div>
        </div>
    </body>
</html>
