<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>List Khach hang</title>
</head>
<body>
<h1 align="center">Customer List</h1>
<table align="center" border="1">
	<thead>
		<tr>
			<th style="width: 150px">Customer ID</th>
			<th style="width: 400px">Customer Name</th>
			<th style="width: 150px">Gender</th>
			<th style="width: 500px">Address</th>
			<th style="width: 300px">Phone Number</th>
			<th style="width: 400px">Email</th>
			<th style="width: 400px"><img alt="" src="img/Edit.png"></th>
			<th style="width: 400px"><img alt="" src="img/Delete.jpg"></th>
		</tr>
	</thead>
	<tbody align="center">
		<c:forEach items="${lskh}" var="ls">
		<tr>
			<td>${ls.ma_khach_hang}</td>
			<td>${ls.ten_khach_hang}</td>
			<td><c:if test="${ls.phai eq 0}">Male</c:if><c:if test="${ls.phai eq 1}">FeMale</c:if></td>
			<td>${ls.dia_chi}</td>	
			<td>${ls.dien_thoai}</td>	
			<td>${ls.email}</td>
			<td><a href="EditCustomerController?id=${ls.ma_khach_hang}">Edit</a></td>	
			<td><a href="EditCustomerController?id=${ls.ma_khach_hang}&action=del">Delete</a></td>
		</tr>
		</c:forEach>
	</tbody>
</table>
<h2 align="center">${result}</h3>
</body>
</html>