<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<h1 align="center">Customer Information</h1>
<form action="EditCustomerController" method="post">
<table align="center" style="text-align: center;" border="1">
	<tbody>
	
		<tr>
			<td style="width: 150px">Customer ID</td>
			<td style="width: 250px"><input style="width: 250px" type="text" name="txtID" value="${txtID}" readonly="readonly"></td>
		</tr>
		<tr>
			<td style="width: 150px">Customer Name</td>
			<td style="width: 250px"><input style="width: 250px" type="text" name="txtName" value="${txtName}"></td>
		</tr>
		<tr>
			<td style="width: 150px">Gender</td>
			<td style="width: 250px" align="center">
			<c:if test="${txtGender eq 0}">
  				<input type="radio" name="txtGender" value="0" checked> Male
  				<input type="radio" name="txtGender" value="1"> Female
  			</c:if>
			<c:if test="${txtGender eq 1}">
  				<input type="radio" name="txtGender" value="0">Male
  				<input type="radio" name="txtGender" value="1" checked> Female
  			</c:if>  			
 			 </td>
		</tr>
		<tr>
			<td style="width: 150px">Address</td>
			<td style="width: 250px"><input style="width: 250px" type="text" name="txtAddress" value="${txtAddress}"></td>
		</tr>
		<tr>
			<td style="width: 150px">Phone Number</td>
			<td style="width: 250px"><input style="width: 250px" type="text" name="txtPhone" value="${txtPhone}"></td>
		</tr>
		<tr>
			<td style="width: 150px">Email</td>
			<td style="width: 250px"><input style="width: 250px" type="text" name="txtEmail" value="${txtEmail}"></td>
		</tr>
		<tr>
			<td colspan="2">
			<input type="submit" name="btnSummit" value="Update">
			<button type="button" name="back" onclick="history.back()">Back</button>
			</td>
		</tr>												
	</tbody>
</table>
</form>
<h2 align="center">${result}</h2>
</body>
</html>