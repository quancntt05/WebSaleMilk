<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <form name="frmThemSua" action="AddProductController" method="POST">
            <table border="1">
                <thead>
                    <tr>
                        <th colspan="2" align="center">Thêm loại Sữa</th>
                    </tr>
                </thead>
                <tbody>
                	<tr>
                		<td>Mã sữa</td>
                		<td><input type="text" name="txtMaSuaMoi" value="" /></td>
                	</tr>
                	
                	<tr>
                		<td>Tên sữa</td>
                		<td><input type="text" name="txtTenSuaMoi" value="" /></td>
                	</tr>                	

                    <tr>
                        <td>
                            Loại sữa:
                        </td>
                        <td>
                            <select name="cboLoai">
                                <c:forEach items="${dsls}" var="ls">
                                    <option value="${ls.maLoaiSua}" ${param.cboLoai==null?"":(param.cboLoai eq ls.maLoaiSua?"selected":"")}>${ls.tenLoaiSua}</option>
                                </c:forEach>
                            </select>
                        </td>
                     </tr>
                     <tr>
                        <td>
                            Hãng sữa:
                        </td>
                        <td>
                            <select name="cboHang">
                                <c:forEach items="${dshs}" var="hs">
                                    <option value="${hs.maHangSua}" ${param.cboHang==null?"":(param.cboHang eq hs.maHangSua?"selected":"")}>${hs.tenHangSua}</option>
                                </c:forEach>
                            </select>
                        </td>
                    </tr>
                	<tr>
                		<td>Trọng lượng</td>
                		<td><input type="text" name="txtTrongLuong" value="" />(gm hoặc ml)</td>
                	</tr>
                	<tr>
                		<td>Đơn giá</td>
                		<td><input type="text" name="txtDonGia" value="" /> VND</td>
                	</tr>
					<tr>
                		<td>Thành phần dinh dưỡng</td>

                		<td><textarea cols="20" rows="1" name="txtTPDinhDuong"/></textarea></td>
                	</tr>
                	<tr>
                		<td>Lợi Ích</td>
                		<td><textarea cols="20" rows="1" name="txtLoiIch"/></textarea></td>
                	</tr>
                	<tr>
                		<td>Hình Ảnh</td>
                		<td><input type="text" name="txtHinh" value="" /></td>
                	</tr>  
                	              	                	
                </tbody>
                 	<tr>
                        <th colspan="2" align="center"><input type="submit" value="Summit" name ="btnSubmit"></th>
                    </tr>
            </table>

        </form>
      <h3 align="center">${result}</h3>
        
    </body>
</html>
