<%-- 
    Document   : thuc-don-loai-sua
--%>

<%@page import="services.ConnectDatabase"%>
<%@page import="java.sql.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%!
    ConnectDatabase database = new ConnectDatabase();
    Connection con = database.connect();
    Statement stm = null;
    ResultSet rs = null;
%>
<%
    stm = con.createStatement();
    rs = stm.executeQuery("select * from loai_sua");
%>
<ul>
    <%while(rs.next()){%>
        <li><a href="index.jsp?maLoai=<%=rs.getString("ma_loai_sua")%>"><%= rs.getString("ten_loai")%></a></li>
    <%}%>
</ul>
