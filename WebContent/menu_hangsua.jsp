<%-- 
    Document   : thuc-don-hang-sua
--%>

<%@page import="java.sql.*"%>
<%@page import="services.ConnectDatabase"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%!
    ConnectDatabase database = null;
    Connection con = null;
    Statement stm = null;
    ResultSet rs = null;
%>
<%
    database = new ConnectDatabase();
    con = database.connect();
    stm = con.createStatement();
    rs = stm.executeQuery("select * from hang_sua");
%>
<ul>
    <%while(rs.next()){%>
        <li><a href="index.jsp?maHang=<%=rs.getString("ma_hang_sua")%>"><%= rs.getString("ten_hang_sua")%></a></li>
    <%}%>
</ul>
